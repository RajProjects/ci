package com.axisbank.custom.redis.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.MessageFormat;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.axisbank.custom.redis.service.PDFService;
import com.axisbank.custom.util.GeneratePDF;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RestController
public class PDFController {

  private final static String CLASSNAME = "[PDFController]";
  private final static String ENTRY = "[ENTRY]";
  private final static String EXIT = "[EXIT]";
  private final static String ERROR = "[ERROR]";

  private static String GENESYS_PDF_REDIS_KEY = "c:genesys:chat:history:download:{0}";

  @Autowired
  private PDFService pdfService;

  static Logger logger = Logger.getLogger(PDFController.class);

  ByteArrayInputStream bis = null;

  /**
   * API to get aha pdf content
   * 
   * @param timestamp
   * @return ResponseEntity -- pdf output
   */
  @RequestMapping(value = "/download", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
  public ResponseEntity<InputStreamResource> downloadPDF(@RequestParam("id") String timestamp) {
    logger.info(CLASSNAME + "[downloadPDF]" + ENTRY);
    logger.info("Redis KEY : " + timestamp);
    HttpHeaders headers = new HttpHeaders();
    try {
      String result = pdfService.getPDFContent(timestamp);
      logger.debug("Redis VALUE  : " + result);
      String str = new String(result.substring(7));
      logger.debug("Redis VALUE after removing junk characters : " + str);
      Type type = new TypeToken<Map<String, String>>() {
      }.getType();
      Gson gson = new Gson();
      Map<String, String> redisMap = gson.fromJson(str, type);
      String chatOutput = redisMap.get("chatOutputStr");
      logger.info("Calling GeneratePDF chatReport()");
      bis = GeneratePDF.chatReport(chatOutput);
      headers.add("Content-Disposition", "attachment; filename=AHA-chat.pdf");
      logger.info("Returning response. Exit PDFController : downloadPDF()");
    } catch (Exception e) {
      logger.info(CLASSNAME + "[downloadPDF]" + ERROR);
      e.printStackTrace();
    }
    logger.info(CLASSNAME + "[downloadPDF]" + EXIT);
    return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
        .body(new InputStreamResource(bis));

  }

  /**
   * <p>
   * API to produce genesys chat history PDF
   * <p/>
   *
   * @param userChannelId userChannelId from request param
   * @return genesys chat history PDF
   * @throws Exception
   */
  @RequestMapping(value = "/genesys/download", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
  public ResponseEntity downloadGenesysPDF(@RequestParam("id") String userChannelId) throws Exception {
    logger.info(CLASSNAME + "[downloadGenesysPDF]" + ENTRY);
    String redisKey = MessageFormat.format(GENESYS_PDF_REDIS_KEY, userChannelId);
    HttpHeaders headers = new HttpHeaders();
    try {
      String responseByteStream = pdfService.getPDFContent(redisKey);
      logger.info("ResponseFromRedis:" + responseByteStream);
      headers.add("Content-Disposition", "attachment; filename=Live-chat.pdf");
      if (null != responseByteStream) {
        bis = GeneratePDF.chatReport(responseByteStream);
        logger.info(CLASSNAME + "[downloadGenesysPDF]" + EXIT);
        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
            .body(new InputStreamResource(bis));
      } else {
        logger.info(CLASSNAME + "[downloadGenesysPDF]" + ERROR);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Chat Expired");
      }
    } catch (Exception e) {
      logger.info(CLASSNAME + "[downloadGenesysPDF]" + ERROR);
      logger.error(e.getStackTrace());
    }
    logger.info(CLASSNAME + "[downloadGenesysPDF]" + ERROR);
    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).headers(headers).body("Unable to download pdf now.");
  }

  @ExceptionHandler(Exception.class)
  void handleBadRequests(HttpServletResponse response) throws IOException {
    logger.info("");
    logger.info("Inside Exception class. BAD REQUEST");
    response.sendError(HttpStatus.BAD_REQUEST.value(),
        "There is some issue with the system. Please try again with valid value");
  }

}
