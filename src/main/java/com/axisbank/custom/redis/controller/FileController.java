package com.axisbank.custom.redis.controller;

import com.axisbank.custom.model.Response;
import com.axisbank.custom.redis.service.FileService;
import com.axisbank.custom.util.Constants;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static com.axisbank.custom.util.Constants.*;

/*
 * <p>
 *  FileService API Controller
 * </p>
 *
 * @author Mohanraj Selvam, Created on Feb 19th.
 */
@ControllerAdvice
@RestController
public class FileController {

  private final static String CLASSNAME = "[FileController]";
  private final static String ENTRY = "[ENTRY]";
  private final static String EXIT = "[EXIT]";
  private final static String ERROR = "[ERROR]";

  @Autowired
  private FileService fileService;

  @Autowired
  private ObjectMapper objectMapper;

  @Autowired
  private Environment environment;

  private static Logger logger = Logger.getLogger(FileController.class);

  /**
   * <p>
   *   Controller for upload file API
   * </p>
   *
   * @param file file part.
   * @param docType document type.
   * @param channelUserId user channel Id.
   * @param fileIndex file index
   * @return ResponseEntity.
   */
  @RequestMapping(value = "/uploadFile",method = RequestMethod.POST)
  public ResponseEntity<Object> uploadFiletoFileSystem(@RequestParam("file") MultipartFile file, @RequestParam("docType")String docType, @RequestParam("channelUserId") String channelUserId, @RequestParam("fileIndex") int fileIndex) throws IOException {
    logger.info(CLASSNAME+"[uploadFiletoFileSystem]"+ENTRY);
    ResponseEntity<Object> response = fileService.copyFileToSharedMountPoint(file,channelUserId,docType,fileIndex);
    logger.info(CLASSNAME+"[uploadFiletoFileSystem]"+EXIT);
    return response;
  }

  /**
   * <p>
   *   MultipartException handler method
   * </p>
   *
   * @param request HttpRequest
   * @param ex Exception
   * @return ResponseEntity with error status.
   */
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MultipartException.class)
  public ResponseEntity<Object> handleFileException(HttpServletRequest request, Throwable ex) {
    logger.info(CLASSNAME+"[handleFileException]"+ENTRY);
    Response uploadFileResponse = new Response("",0,Constants.ERROR,1,FILE_RESPONSE_INVALID_FILE_SIZE_ERROR_CODE,environment.getProperty(INVALID_FILE_SIZE_RESPONSE));
    logger.info(CLASSNAME+"[handleFileException]"+EXIT);
    return new ResponseEntity<>(uploadFileResponse,HttpStatus.OK);
  }

  /**
   * <p>
   *   MissingServletRequestParameterException handler method
   * </p>
   *
   * @param request HttpRequest
   * @param ex Exception
   * @return ResponseEntity with error status.
   */
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler({MissingServletRequestParameterException.class, MethodArgumentTypeMismatchException.class, MissingServletRequestPartException.class})
  ResponseEntity<Object> handleMissingServletRequestParameterExceptionn(HttpServletRequest request, Throwable ex) {
    logger.info(CLASSNAME+"[handleMissingServletRequestParameterExceptionn]"+ENTRY);
    Response uploadFileResponse = new Response("",0,Constants.ERROR,1,FILE_RESPONSE_BAD_REQ_ERROR_CODE,environment.getProperty(BAD_REQUEST_RESPONSE));
    logger.info(CLASSNAME+"[handleMissingServletRequestParameterExceptionn]"+EXIT);
    return new ResponseEntity<>(uploadFileResponse,HttpStatus.OK);
  }

  /**
   * <p>
   *   IOException handler method
   * </p>
   *
   * @param request HttpRequest
   * @param ex Exception
   * @return ResponseEntity with error status.
   */
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  @ExceptionHandler(IOException.class)
  ResponseEntity<Object> handleIOException(HttpServletRequest request, Throwable ex) {
    logger.info(CLASSNAME+"[handleIOException]"+ENTRY);
    Response uploadFileResponse = new Response("",0,Constants.ERROR,1,FILE_RESPONSE_IO_ERROR_CODE,environment.getProperty(INVALID_FILE_ERROR_RESPONSE));
    logger.info(CLASSNAME+"[handleIOException]"+EXIT);
    return new ResponseEntity<>(uploadFileResponse,HttpStatus.OK);
  }
}
