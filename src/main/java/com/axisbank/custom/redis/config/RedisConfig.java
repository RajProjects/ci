package com.axisbank.custom.redis.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import com.axisbank.custom.redis.service.PDFServiceImpl;

@Configuration
@ComponentScan("com.axisbank.custom.redis")
public class RedisConfig {
	
	@Value("${pdfdownload.redis.host}")
	private String redisHost;
	
	@Value("${pdfdownload.redis.port}")
	private String redisPortString;
	
	static Logger logger = Logger.getLogger(PDFServiceImpl.class);
	
	@Bean
	JedisConnectionFactory jedisConnectionFactory() {
		JedisConnectionFactory factory = new JedisConnectionFactory(); 
		logger.debug("REDIS HOST : "+redisHost);
		logger.debug("REDIS PORT : "+redisPortString);
		int redisPort = Integer.parseInt(redisPortString);
		logger.debug("REDIS PORT INT: "+redisPort);
        factory.setHostName(redisHost);
		factory.setPort(redisPort);  
        
        return factory;
	}

	@Bean
	public RedisTemplate<String, Object> redisTemplate() {
		final RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
		template.setConnectionFactory(jedisConnectionFactory());
		
		logger.info("################################");
		logger.info("PORT : "+jedisConnectionFactory().getPort());
		logger.info("HOSTNAME : "+jedisConnectionFactory().getHostName());
		logger.info("################################");
		template.setValueSerializer(new StringRedisSerializer());
		
		return template;
	}
}
