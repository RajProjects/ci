package com.axisbank.custom.redis.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class PDFServiceImpl implements PDFService {

  static Logger logger = Logger.getLogger(PDFServiceImpl.class);

  @Autowired
  private RedisTemplate<String, String> redisTemplate;

  /*
   * (non-Javadoc)
   * 
   * @see
   * com.axisbank.custom.redis.service.PDFService#getPDFContent(java.lang.String)
   */
  public String getPDFContent(String redisKey) {
    logger.info("Inside PDFServiceImpl : getPDFContent() method : Getting value from Redis using redis key");
    return redisTemplate.opsForValue().get(redisKey);
  }
}