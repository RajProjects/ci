/*
 * (C) Copyright 2016 Active Intelligence Pte Ltd (http://active.ai/).
 *
 * This software is the confidential and proprietary information of Active Intelligence.
 * ("Confidential Information"). You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you entered into with Active
 * Intelligence
 *
 */
package com.axisbank.custom.redis.service;

import com.axisbank.custom.model.DocType;
import com.axisbank.custom.model.Response;
import com.axisbank.custom.util.Constants;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;

import static com.axisbank.custom.util.Constants.*;

/*
 * <p>
 *  FileServiceImplementation Service implementation
 * </p>
 *
 * @author Mohanraj Selvam, Created on Feb 14th.
 */
@Repository
public class FileServiceImplementation implements FileService {

  private static Logger logger = Logger.getLogger(FileServiceImplementation.class);

  @Value("${FILE_UPLOAD_SUPPORTED_MEDIA_TYPE:application/pdf}")
  private String FILE_UPLOAD_SUPPORTED_MEDIA_TYPE;

  @Value("${FILE_UPLOAD_SIZE_LIMIT_IN_MB:5}")
  private String FILE_UPLOAD_SIZE_LIMIT_IN_MB;

  @Autowired
  private Environment environment;

  @Value("${FILE_UPLOAD_SUPPORTED_EXTENTION:pdf}")
  private String FILE_UPLOAD_SUPPORTED_EXTENTION;

  @Override
  public ResponseEntity<Object> copyFileToSharedMountPoint(MultipartFile file, String channelUserId, String docType, int fileIndex) throws IOException {
    logger.info("file Name "+file.getOriginalFilename());
    String documentType = DocType.getDocTypeByString(docType);
    Response response = null;
    ResponseEntity <Object> responseEntity = null;
    if (!isValidFileExtention(file.getOriginalFilename(),FILE_UPLOAD_SUPPORTED_EXTENTION)){
      logger.info("Invalid File"+file.getName()+" and format "+file.getContentType());
      response =  new Response(file.getOriginalFilename(),fileIndex,ERROR,1,FILE_RESPONSE_INVALID_FORMAT_ERROR_CODE,environment.getProperty(INVALID_FILE_FORMAT_RESPONSE));
      responseEntity = new ResponseEntity<Object>(response, HttpStatus.OK);
      logger.info("File uploaded failed --> "+FILE_RESPONSE_INVALID_FORMAT_ERROR_CODE);
      return responseEntity;
    }
    if (null != documentType &&file.getSize() > 0){
      if (file.getContentType().trim().equalsIgnoreCase(FILE_UPLOAD_SUPPORTED_MEDIA_TYPE.trim())) {
        if (file.getSize()<=( Long.parseLong(FILE_UPLOAD_SIZE_LIMIT_IN_MB) *1024 *1024)) {
          byte[] bytes = file.getBytes();
          String mountPath = System.getProperty(SHARED_MOUNT_POINT_ENV);
          if (!StringUtils.isEmpty(mountPath)) {
            Path path = Paths.get(mountPath +channelUserId+"_"+docType+"_"+ file.getOriginalFilename());
            Files.write(path, bytes);
            response = new Response(file.getOriginalFilename(),SUCCESS,0,channelUserId+"_"+docType+"_"+ file.getOriginalFilename(),fileIndex);
            responseEntity = new ResponseEntity<Object>(response, HttpStatus.OK);
            logger.info("File uploaded to --> "+path.toString());
          } else {
            logger.info("Invalid shared path configured");
            logger.info("File upload failed --> "+INTERNAL_SERVER_ERROR_RESPONSE);
            response = new Response(file.getOriginalFilename(),fileIndex, Constants.ERROR,1,FILE_RESPONSE_IO_ERROR_CODE,environment.getProperty(INTERNAL_SERVER_ERROR_RESPONSE));
            responseEntity = new ResponseEntity<Object>(response, HttpStatus.OK);
          }

        } else {
          logger.info("file Size "+file.getSize());
          logger.info("File uploaded failed --> "+FILE_RESPONSE_INVALID_FILE_SIZE_ERROR_CODE);
          response = new Response(file.getOriginalFilename(),fileIndex, Constants.ERROR,1,FILE_RESPONSE_INVALID_FILE_SIZE_ERROR_CODE,environment.getProperty(INVALID_FILE_SIZE_RESPONSE));
          responseEntity = new ResponseEntity<Object>(response, HttpStatus.OK);
        }
      }
      else {
        logger.info("UnSupported  format "+file.getContentType());
        response =  new Response(file.getOriginalFilename(),fileIndex,ERROR,1,FILE_RESPONSE_INVALID_FORMAT_ERROR_CODE,environment.getProperty(INVALID_FILE_FORMAT_RESPONSE));
        responseEntity = new ResponseEntity<Object>(response, HttpStatus.OK);
        logger.info("File uploaded failed --> "+FILE_RESPONSE_INVALID_FORMAT_ERROR_CODE);
      }
    }else {
      response = new Response(file.getOriginalFilename(),fileIndex,ERROR,1,FILE_RESPONSE_INVALID_DOC_ERROR_CODE,environment.getProperty(INVALID_DOC_TYPE_RESPONSE));
      responseEntity = new ResponseEntity<Object>(response, HttpStatus.OK);
      logger.info("Invalid doc type --> "+docType);
      logger.info("File uploaded failed --> "+FILE_RESPONSE_INVALID_DOC_ERROR_CODE);
    }
    return responseEntity;
  }
  /**
   * <p>Service method to check file extention</p>
   *
   * @param fileName from MultiPart file
   * @param expectedExtention configured extention
   * @return status of validation
   */
  private static boolean isValidFileExtention(String fileName,String expectedExtention) {
    String [] fileParts = fileName.split(Pattern.quote("."));
    String extention = fileParts[fileParts.length-1];
    return expectedExtention.equalsIgnoreCase(extention);
  }
}
