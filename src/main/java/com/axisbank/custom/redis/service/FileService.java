/*
 * (C) Copyright 2016 Active Intelligence Pte Ltd (http://active.ai/).
 *
 * This software is the confidential and proprietary information of Active Intelligence.
 * ("Confidential Information"). You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you entered into with Active
 * Intelligence
 *
 */
package com.axisbank.custom.redis.service;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/*
 * <p>
 *  FileService interface
 * </p>
 *
 * @author Mohanraj Selvam, Created on Feb 14th.
 */
public interface FileService {

  /**
   * <p>
   *   Copies requested file in to common mount point.
   * </p>
   * @param file File from Request
   * @return Status of file upload.
   */
  ResponseEntity<Object> copyFileToSharedMountPoint(MultipartFile file, String channelUserId, String docType,int fileIndex) throws IOException;
}
