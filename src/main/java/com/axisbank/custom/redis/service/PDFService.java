package com.axisbank.custom.redis.service;

public interface PDFService {
  
/**
 * 
 * Get the PDF content from redis
 * 
 * @param redisKey
 * @return String
 */
  String getPDFContent(String redisKey);
}
