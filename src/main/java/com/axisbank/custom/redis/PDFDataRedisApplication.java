package com.axisbank.custom.redis;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;

import javax.servlet.MultipartConfigElement;

@PropertySource(value = {"classpath:message.properties","classpath:application.properties"})
@SpringBootApplication
public class PDFDataRedisApplication extends SpringBootServletInitializer {

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(PDFDataRedisApplication.class);
  }

  private static Logger logger = Logger.getLogger(PDFDataRedisApplication.class);

  public static void main(String[] args)  {
    SpringApplication.run(PDFDataRedisApplication.class, args);
  }

  @Bean
  public MultipartConfigElement multipartConfigElement() {
    MultipartConfigFactory factory = new MultipartConfigFactory();
    factory.setMaxFileSize(-1);
    factory.setMaxRequestSize(-1);
    return factory.createMultipartConfig();
  }

}