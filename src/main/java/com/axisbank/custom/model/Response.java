/*
 * (C) Copyright 2016 Active Intelligence Pte Ltd (http://active.ai/).
 *
 * This software is the confidential and proprietary information of Active Intelligence.
 * ("Confidential Information"). You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you entered into with Active
 * Intelligence
 *
 */
package com.axisbank.custom.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * <p>
 * Upload file Response POJO class
 * </p>
 *
 * @author Mohanraj Selvam, Created on Oct 22nd.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {
  @JsonProperty("fileName")
  private String fileName;
  @JsonProperty("fileIndex")
  private int fileIndex;
  @JsonProperty("status")
  private String status;
  @JsonProperty("statusCode")
  private int statusCode;
  @JsonProperty("filePath")
  private String filePath;
  @JsonProperty("errorMessage")
  private String errorMessage;
  @JsonProperty("errorCode")
  private String errorCode;

  public Response(String fileName, String status, int statusCode, String filePath, int fileIndex){
    this.fileName = fileName;
    this.status = status;
    this.statusCode = statusCode;
    this.filePath = filePath;
    this.fileIndex = fileIndex;
  }

  public Response(String fileName, int fileIndex, String status, int statusCode,String errorCode, String errorMessage){
    this.fileName = fileName;
    this.status = status;
    this.statusCode = statusCode;
    this.errorCode = errorCode;
    this.errorMessage = errorMessage;
    this.fileIndex = fileIndex;

  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public int getStatusCode() {
    return statusCode;
  }

  public void setStatusCode(int statusCode) {
    this.statusCode = statusCode;
  }

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public String getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(String errorCode) {
    this.errorCode = errorCode;
  }

  @Override
  public String toString() {
    return "Response{" +
            "fileName='" + fileName + '\'' +
            ", status='" + status + '\'' +
            ", statusCode=" + statusCode +
            ", filePath='" + filePath + '\'' +
            ", errorMessage='" + errorMessage + '\'' +
            ", errorCode='" + errorCode + '\'' +
            '}';
  }
}
