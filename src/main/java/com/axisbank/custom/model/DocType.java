package com.axisbank.custom.model;

/**
 * Document type enum
 */
public enum DocType  {
  CUSTOMER_DISPUTE_FORM("CDF"),PASSPORT("PSPT"),FIR("FIR"),POLICE_INTIMATION_LETTER("PIL"),INCIDENT_LETTER("IL"),OTHERS("OTH");

  private String type;

  DocType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  /**
   *
   * @param type
   * @return
   */
  public static String getDocTypeByString(String type){
    for(DocType e : DocType.values()){
      if(type.equals(e.type)) return e.name();
    }
    return null;
  }
}
