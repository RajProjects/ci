/*
 * (C) Copyright 2016 Active Intelligence Pte Ltd (http://active.ai/).
 *
 * This software is the confidential and proprietary information of Active Intelligence.
 * ("Confidential Information"). You shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement you entered into with Active
 * Intelligence
 *
 */
package com.axisbank.custom.util;

/**
 * App constants
 */
public interface Constants {

  /*
  API RESPONSE STATUS
   */
  String SUCCESS = "success";
  String ERROR = "error";

  String FILE_RESPONSE_IO_ERROR_CODE = "ERR_IO";
  String FILE_RESPONSE_BAD_REQ_ERROR_CODE = "400";
  String FILE_RESPONSE_INVALID_FILE_SIZE_ERROR_CODE = "INV_SZ";
  String FILE_RESPONSE_UNKNOWN_ERROR_CODE = "UNKN";
  String FILE_RESPONSE_INVALID_DOC_ERROR_CODE = "INV_DOC";
  String FILE_RESPONSE_INVALID_FORMAT_ERROR_CODE = "INV_FRMT";

  /**
   * Message Constants
   */
  String INVALID_FILE_SIZE_RESPONSE  = "INVALID_FILE_SIZE_RESPONSE";
  String INVALID_FILE_FORMAT_RESPONSE = "INVALID_FILE_FORMAT_RESPONSE";
  String INVALID_DOC_TYPE_RESPONSE = "INVALID_DOC_TYPE_RESPONSE";
  String INTERNAL_SERVER_ERROR_RESPONSE = "INTERNAL_SERVER_ERROR_RESPONSE";
  String BAD_REQUEST_RESPONSE = "BAD_REQUEST_RESPONSE";
  String INVALID_FILE_ERROR_RESPONSE = "INVALID_FILE_ERROR_RESPONSE";

  String SHARED_MOUNT_POINT_ENV = "shared.mount.path";

}
