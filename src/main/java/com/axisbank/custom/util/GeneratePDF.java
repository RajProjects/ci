package com.axisbank.custom.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.helper.W3CDom;
import org.jsoup.nodes.Document;
import org.xhtmlrenderer.pdf.ITextRenderer;

public class GeneratePDF {

  static Logger logger = Logger.getLogger(GeneratePDF.class);

  private static final int WIDTH = 1420;
  private static final int HEIGHT = 900;

  public static ByteArrayInputStream chatReport(String result) throws Exception {
    logger.info("Inside GeneratePDF : chatReport()");
    Document doc = Jsoup.parse(result);
    W3CDom w3CDom = new W3CDom();
    org.w3c.dom.Document w3cDoc = w3CDom.fromJsoup(doc);
    ITextRenderer renderer = new ITextRenderer(WIDTH, HEIGHT);
    renderer.setDocument(w3cDoc, null);
    ByteArrayOutputStream os = new ByteArrayOutputStream();
    renderer.layout();
    renderer.createPDF(os);
    os.close();
    logger.info("Inside GeneratePDF : chatReport() : Sending OutPut stream");
    return new ByteArrayInputStream(os.toByteArray());
  }
}